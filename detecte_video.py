# importation des bibliothèques
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
from imutils.video import VideoStream
from pygame import mixer
import numpy as np
import cv2

mixer.init()
sound = mixer.Sound("alarm.mp3")

# importer l'architecture et les poids du modèle de détection de visage
architecture = r"face_detector\deploy.prototxt"
poids = r"face_detector\weights.caffemodel"
faceModel = cv2.dnn.readNet(architecture, poids)

# importer le model de détection de mask
maskModel = load_model(r"model\best_model.hdf5")

# initialiser le capteur de flux vidéo
vs = VideoStream(src=0).start()

'''
implémenter une fonction pour la détection du masque 
qui retourne la localisation des visages et les prédictions 
'''
def detect_mask(frame, faceModel, maskModel):
    # initialiser des listes pour les visages, leurs emplacements et les prédictions
    faces = []
    localisations = []
    predictions = []

    (h, l) = frame.shape[:2]
    # {h:hauteur , l:largeur}
    
    # prétraitement d'image
    blob = cv2.dnn.blobFromImage(frame, scalefactor=1.0, size=(300,300), mean=(104, 177, 123))

    # obtenir les détections de visage
    faceModel.setInput(blob)
    detection = faceModel.forward()

    for i in range(detection.shape[2]):
        confidence = detection[0, 0, i, 2] 
        if confidence > 0.5:
            #calculer les délimitations de la boîte
            boxes = detection[0, 0, i, 3:] * np.array([l,h, l,h])

            (gauche, haut, droite, bas) = boxes.astype('int')
            (gauche, haut) = (max(0, gauche)), max(0, haut)
            (droite, bas) = (min(l-1, droite), min(h-1, bas))

            face = frame[haut:bas , gauche:droite]

            '''
            prétraitement pour l'image du visage de la même manière que nous l'avons fait pendant l'entrainement
            notre modèle a été formé sur des images RGB mais OpenCV représente
            les images dans l'ordre BGR, alors permutez les canaux, 
            puis redimensionnez 224x224 (les dimensions d'entrée pour MobileNetV2)
            '''
            face = cv2.cvtColor(face, cv2.COLOR_BGR2RGB)
            face = cv2.resize(face, dsize=(224, 224))
            face = img_to_array(face)      
            face = preprocess_input(face)  

            faces.append(face)
            localisations.append((gauche, haut, droite, bas))

    # nous faisons des prédictions si au moins un visage est détecté
    if len(faces) > 0:
        faces = np.array(faces, dtype='float32')
        predictions = maskModel.predict(faces, batch_size=32)
    
    return(localisations, predictions)

while True:
    frame = vs.read()
    (locs, preds) = detect_mask(frame, faceModel, maskModel)

    for (box, pred) in zip (locs, preds):
        (with_mask, without_mask) = pred
        if with_mask > without_mask:
            classe = "Avec Mask"
            couleur = (0, 255, 0)
        else:
            classe = "Sans Mask" 
            couleur = (0, 0, 255)
            sound.play()

        pourcentage = "%.2f"%(max(with_mask, without_mask) * 100)
        classe = "{} : {}".format(classe, pourcentage)

        (gauche, haut, droite, bas) = box

        # tracer un rectangle autour des visages
        cv2.rectangle(frame, (gauche, haut), (droite, bas), couleur, 2)
        # afficher la classe du visage
        cv2.putText(frame, classe, (gauche, haut-15), cv2.FONT_HERSHEY_SIMPLEX, 0.45, couleur, 2)

    cv2.imshow("Output", frame)    
    if cv2.waitKey(1) == ord('q'): 
        break
cv2.destroyAllWindows()
vs.stop()             